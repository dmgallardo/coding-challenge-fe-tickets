# Tickets Coding Challenge - Frontend

## What do you have to do?
To develop an app to show the tickets made in a city. These tickets will have different status, locations, infractor name and vehicle information (very minimum information just for the excercise). Location will be latitude and longitude. Status are: pending, aborted, approved, paid.


## Rules of the challenge
* It should be solved on a weekend (3 days).
* The solution should take between 8hr to 10hr.
* The project needs to be published on github or bitbucket personal repository.
* The solution will be presented in front of the Development Team 1 or 2 days after the presentation.
* Tech Stack to be used on the solution (One of them):
* React (preferred)
* Angular

## Pages
1. Home / Map.
2. List of Tickets.
3. Add a new Ticket.

### Home / Dashboard
This page needs to show a map with the tickets geolocated on it. Also when the user click on it, it should show the information of the ticket.

### List of Tickets
This page should include the list of all tickets. The user may be able to filter by status or vehicle identifier.

### Add a new Ticket (Optional)
Form to create a new Ticket. This form should save the data with POST endpoints.

## Code
You need to fork this repository in order to complete the exercise.

#### Commands to setup the project and install the json server
* Install dependencies: ```npm install```
* Init the database from Json file: ```npm run server```

#### Endpoints
* List all tickets: ```GET http://localhost:3001/tickets```
* Filter tickets by vehicle identifier: ```http://localhost:3001/tickets?vehicle.identifier=XHJ123```
* Filter tickets by status: ```http://localhost:3001/tickets?status=Pending```
* Show one ticket: ```GET http://localhost:3001/tickets/:ID```
* Add a new ticket:  ```POST http://localhost:3001/tickets```

Post Data example:
 ```
{
    "lat": -31.655510,
    "lng": -60.718874,
    "person": {
        "first_name": "Carlos", 
        "last_name": "Lopez" 
    },
    "vehicle": {
        "identifier": "XHJ123"
    },
    "status": "Pending"
}
```

## Clarifications
* You can use google maps to show the tickets. (Google Maps apikey: AIzaSyBUxLpWkdrwadEajpgwCulgNEr0G-8kqvI).
* Json file with tickets information is in this repository on db.json file.
* You are able to use any library for the UI components (Bootstrap, Material, etc).
* You need to have installed node and npm in order to execute the json-server.

## Extra (not mandatory)
* You can add the edition of the tickets
* Documentation
* Use git flow